# GitLab CI template for Renovate

Automate your dependency updates with [Renovate](https://www.mend.io/renovate/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component)
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # include the component
  - component: $CI_SERVER_FQDN/to-be-continuous/renovate/gitlab-ci-renovate@1.4.1
```

### Use as a CI/CD template (legacy)

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # include the template
  - project: "to-be-continuous/renovate"
    ref: "1.4.1"
    file: "/templates/gitlab-ci-renovate.yml"
```

## Configuration

The Renovate template uses some global configuration used throughout all jobs.

| Input / Variable           | Description                                                                                                                                               | Default value                                      |
| -------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------- |
| `image` / `RENOVATE_IMAGE` | The Docker image used to run Renovate                                                                                                                     | `registry.hub.docker.com/renovate/renovate:latest` |
| `onboarding-config` / `RENOVATE_ONBOARDING_CONFIG` | Renovate configuration to use for onboarding Renovate                                                                                                                     | [see below](#default-onboarding-configuration) |
| :lock: `RENOVATE_TOKEN`    | A GitLab access token to allow Renovate crawl your projects. [See doc](https://docs.renovatebot.com/modules/platform/gitlab/#authentication)              | _none_                                             |
| :lock: `GITHUB_COM_TOKEN`  | A GitHub access token to allow Renovate fetch changelogs. [See doc](https://docs.renovatebot.com/getting-started/running/#githubcom-token-for-changelogs) | _none_                                             |
| `force-execution` / `RENOVATE_FORCE_EXECUTION` | True to force execution of renovate (to bypass skip ci)                   | `false` |

This template will help you using [Renovate](https://www.mend.io/renovate/) from a GitLab project to
automate your dependency updates within your groups or projects.
On the contrary to other to-be-continuous templates, this one should be used in a separate project that
will be in charge of crawling all your other projects.

Upon including the template, carefuly follow [Renovate's documentation](https://docs.renovatebot.com/) to
configure the bot accordingly. Pay attention to the following:

- ~~Remember to set the [platform](https://docs.renovatebot.com/self-hosted-configuration/#platform) parameter
  to `gitlab` in your configuration.~~
- [GitLab platform integration](https://docs.renovatebot.com/modules/platform/gitlab/) requires that you
  declare a `RENOVATE_TOKEN` variable with an access token.
- You'll also probaly need to declare a `GITHUB_COM_TOKEN` variable, holding a GitHub access token
  (for [fetching changelogs](https://docs.renovatebot.com/getting-started/running/#githubcom-token-for-changelogs))

### Default Renovate configuration

This template is designed to be run on GitLab.

Defaults values are set to manage GitLab features :

| Renovate variable                                                                                            | Value                                     |
| ------------------------------------------------------------------------------------------------------------ | ----------------------------------------- |
| [`RENOVATE_PLATFORM`](https://docs.renovatebot.com/self-hosted-configuration/#platform)                      | `gitlab`                                  |
| [`RENOVATE_ENDPOINT`](https://docs.renovatebot.com/self-hosted-configuration/#endpoint)                      | `$CI_API_V4_URL`                          |
| [`RENOVATE_AUTODISCOVER_FILTER`](https://docs.renovatebot.com/self-hosted-configuration/#autodiscoverfilter) | `${CI_PROJECT_ROOT_NAMESPACE}/**`         |
| [`RENOVATE_REGISTRY_ALIASES`](https://docs.renovatebot.com/configuration-options/#registryaliases)           | `{"$$CI_SERVER_FQDN": "$CI_SERVER_FQDN"}` |
| [`RENOVATE_BINARY_SOURCE`](https://docs.renovatebot.com/self-hosted-configuration/#binarysource)             | `install`                                 |
| [`RENOVATE_LOG_FILE`](https://docs.renovatebot.com/config-overview/#logging-variables)                       | `renovate-log.ndjson`                     |
| [`RENOVATE_LOG_FILE_LEVEL`](https://docs.renovatebot.com/config-overview/#logging-variables)                 | `debug`                                   |

#### Default onboarding configuration
`
The default [onboarding configuration](https://docs.renovatebot.com/self-hosted-configuration/#onboardingconfig) is suitable for projects using to-be-continuous:

- looks for user-defined inputs and variables in your `.gitlab-ci.yml` defining Docker images (ex: `SOMETOOL_IMAGE: docker.io/sometool:1.2.2`)

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "config:recommended",
    ":dependencyDashboard"
  ],
  "labels": [
    "dependencies"
  ],
  "customManagers": [
    {
      "customType": "regex",
      "fileMatch": [ "\\.gitlab-ci\\.ya?ml$" ], 
      "matchStrings": [ "\\s?_IMAGE:\\s['\"](?<registryUrls>.*?)\\/(?<depName>.*?):(?<currentValue>.*)['\"]" ], 
      "datasourceTemplate": "docker" 
    },
    {
      "customType": "regex",
      "fileMatch": [ "\\.gitlab-ci\\.ya?ml$" ], 
      "matchStrings": [ "\\s?image:\\s['\"](?<registryUrls>.*?)\\/(?<depName>.*?):(?<currentValue>.*)['\"]" ], 
      "datasourceTemplate": "docker" 
    }
  ] 
}
```
### Dry-run implementation details

Depending on the source of a pipeline, the template will either perform your dependency updates (create/update/delete branches and MRs)
or [perform a dry run](https://docs.renovatebot.com/self-hosted-configuration/#dryrun) (to preview the behavior of Renovate with logs, without making any changes to your repositories).

The real dependency updates is triggered only when:

- the pipeline is started from the **web interface** (menu _Build > Pipelines_, click _Run pipeline_),
- or the pipeline is started from a **scheduled run** (menu _Build > Pipeline schedules_).

In any other case (regular Git commit or else) the template will perform a dry run.

:warning: Dry-run behavior is enabled/disabled with the `RENOVATE_DRY_RUN` environment variable.
Please don't try to override it or you might break the default template implementation.
